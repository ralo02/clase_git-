#include <stdio.h>
int main(int argc, char const *argv[]) {
  char arreglo[6];
  arreglo[0]='A';
  arreglo[1]='B';
  arreglo[2]='C';
  arreglo[3]='D';
  arreglo[4]='E';
  arreglo[5]='F';
  printf("EL VALOR DEL SUBINDICE 0 ES %C\n", arreglo[0]);
  printf("EL VALOR DEL SUBINDICE 1 ES %C\n", arreglo[1]);
  printf("EL VALOR DEL SUBINDICE 2 ES %C\n", arreglo[2]);
  printf("EL VALOR DEL SUBINDICE 3 ES %C\n", arreglo[3]);
  printf("EL VALOR DEL SUBINDICE 4 ES %C\n", arreglo[4]);
  printf("EL VALOR DEL SUBINDICE 5 ES %C\n", arreglo[5]);
  return 0;
}
